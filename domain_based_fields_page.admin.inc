<?php

/**
 * @file
 * A domain based field setting form.
 */

/**
 * Create form for selecting content types.
 */
function domain_based_fields_form($form, $form_state, $domain) {
  $domain_id = $domain['domain_id'];
  $fields_data = domain_based_fields_get_fields($domain_id);
  $form['content_type_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content type field configuration'),
  );
  $form['content_type_fieldset']['#tree'] = TRUE;
  foreach (node_type_get_types() as $type) {
    $field_option = domain_based_fields_dropdown_options($type->type);
    if (isset($fields_data[$type->type]) && $fields_data[$type->type]['fields'] != '') {
      foreach ($fields_data[$type->type]['fields'] as $v) {
        $field_default_value[] = $v->field;
      }
    }
    else {
      $field_default_value = array();
    }
    $form['content_type_fieldset'][$type->type] = array(
      '#type' => 'fieldset',
      '#title' => $type->name,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['content_type_fieldset'][$type->type]['fields'] = array(
      '#title' => t('Select fields for %name (%domain).', array('%name' => $domain['sitename'], '%domain' => $domain['path'])),
      '#type' => 'checkboxes',
      '#options' => $field_option,
      '#description' => t('List of all the field related to this content type.'),
      '#default_value' => $field_default_value,
    );
  }
  $form['domain_id'] = array(
    '#type' => 'value',
    '#value' => $domain_id,
  );
  $form['domain_name'] = array(
    '#type' => 'value',
    '#value' => $domain['sitename'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Submit handler for custom_domain_ct_form.
 */
function domain_based_fields_form_submit(&$form, &$form_state) {
  $fields = array();
  $values = $form_state['values']['content_type_fieldset'];
  $domain_id = $form_state['values']['domain_id'];
  $domain_name = $form_state['values']['domain_name'];

  if (is_array($values) && count($values)) {
    $i = 0;
    $j = 0;
    foreach ($values as $key => $value) {
      $selected_field = array_filter($value['fields']);
      if (is_array($selected_field) && count($selected_field)) {
        $fields[$i]['type'] = $key;
        foreach ($selected_field as $k => $v) {
          $instance_info = field_info_instance('node', $k, $key);
          $fields[$i]['fields_name'][$j]['field'] = $k;
          $fields[$i]['fields_name'][$j]['required'] = $instance_info['required'];
          $j++;
        }
        $i++;
      }
    }
    $fields_data = json_encode($fields);
    $id = db_merge('domain_based_fields')
      ->key(array('domain_id' => $domain_id))
      ->fields(array(
        'fields_data' => $fields_data,
      ))
      ->execute();
    if ($id) {
      drupal_set_message(t('Fields settings have been saved for %name .', array('%name' => $domain_name)));
    }
  }
  else {
    drupal_set_message(t('The settings have been saved.'));
  }
}

/**
 * Function to get all the field.
 */
function domain_based_fields_dropdown_options($type = '') {
  $field_name = array();
  $fields = field_info_instances("node", $type);
  foreach ($fields as $key => $value) {
    $field_name[$key] = $value['label'];
  }

  return $field_name;
}
