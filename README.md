INTRODUCTION
------------

This module provide additional functionality to the admin user for accessing
fields based on domain.

The main functionality of this module to provide an admin interface from which
we have to set particular fields based on the active domain for editing and /
or adding node content.

REQUIREMENTS
------------

This module require Domain Access module:
https://www.drupal.org/project/domain

CONFIGURATION
-------------
 1) Download and enable module.
 2) Go to -> Structure -> Domain -> click edit domain from the list of domain
    for which you want to show particular fields.
 3) Now you can add select multiple fields from the list based on content type
    and save.
 4) Now you can check by editing or adding content.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
